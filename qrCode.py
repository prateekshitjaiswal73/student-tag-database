import qrcode
from tkinter import *
from tkinter import messagebox
from PIL import ImageTk
import time

wn = Tk()
wn.title('AKATSUKI')
wn.geometry('700x700')
wn.config(bg='SteelBlue3')

def generateCode_student():
    qr = qrcode.QRCode(version = 5,
                   box_size = 10,
                   border = 5)
    qr.add_data(f"https://vasu07017.github.io/{text_student.get()}/")
    qr.make(fit = True)
    img = qr.make_image()
    img.save(f'./Student/{name_student.get()}.png')

    messagebox.showinfo("Akatsuki QR Code Generator","QR Code is saved successfully!")

def generateCode_faculty():
    qr = qrcode.QRCode(version = 5,
                   box_size = 10,
                   border = 5)
    qr.add_data(f"https://github.com/vasu07017/database/blob/main/{text_faculty.get()}.html")
    qr.make(fit = True)
    img = qr.make_image()
    img.save(f'./Faculty/{name_faculty.get()}.png')

    messagebox.showinfo("Akatsuki QR Code Generator","QR Code is saved successfully!")


headingFrame = Frame(wn,bg="azure",bd=5)
headingFrame.place(relx=0.15,rely=0.05,relwidth=0.7,relheight=0.1)
headingLabel = Label(headingFrame, text="AKATSUKI", bg='red', font=('Times',20,'italic'))
headingLabel.place(relx=0,rely=0, relwidth=1, relheight=1)

Frame1 = Frame(wn,bg="SteelBlue3")
Frame1.place(relx=0.1,rely=0.15,relwidth=0.7,relheight=0.3)

lable1 = Label(Frame1,text="Enter the text/URL student: ",bg="SteelBlue3",fg='azure',font=('Courier',13,'bold'))
lable1.place(relx=0.05,rely=0.2, relheight=0.08)

text_student = Entry(Frame1,font=('Century 12'))
text_student.place(relx=0.05,rely=0.4, relwidth=1, relheight=0.2)

Frame9 = Frame(wn,bg="SteelBlue3")
Frame9.place(relx=0.1,rely=0.35,relwidth=0.7,relheight=0.3)

lable9 = Label(Frame9,text="Enter the text/URL for faculty: ",bg="SteelBlue3",fg='azure',font=('Courier',13,'bold'))
lable9.place(relx=0.05,rely=0.2, relheight=0.08)

text_faculty = Entry(Frame9,font=('Century 12'))
text_faculty.place(relx=0.05,rely=0.4, relwidth=1, relheight=0.2)


Frame3 = Frame(wn,bg="SteelBlue3")
Frame3.place(relx=0.1,rely=0.55,relwidth=0.7,relheight=0.3)

lable3 = Label(Frame3,text="Enter the name of the QR Code student: ",bg="SteelBlue3",fg='azure',font=('Courier',13,'bold'))
lable3.place(relx=0.05,rely=0.2, relheight=0.08)


name_student = Entry(Frame3,font=('Century 12'))
name_student.place(relx=0.05,rely=0.4, relwidth=1, relheight=0.2)

Frame11 = Frame(wn,bg="SteelBlue3")
Frame11.place(relx=0.1,rely=0.75,relwidth=0.7,relheight=0.3)

lable11 = Label(Frame11,text="Enter the name of the QR Code for faculty: ",bg="SteelBlue3",fg='azure',font=('Courier',13,'bold'))
lable11.place(relx=0.05,rely=0.2, relheight=0.08)


name_faculty = Entry(Frame11,font=('Century 12'))
name_faculty.place(relx=0.05,rely=0.4, relwidth=1, relheight=0.2)

button = Button(wn, text='Generate Code for Student',font=('Courier',13,'normal'),command=generateCode_student)
button.place(relx=0,rely=0.95, relwidth=0.45, relheight=0.05)

button = Button(wn, text='Generate Code for Faculty',font=('Courier',13,'normal'),command=generateCode_faculty)
button.place(relx=0.55,rely=0.95, relwidth=0.45, relheight=0.05)

wn.mainloop()
